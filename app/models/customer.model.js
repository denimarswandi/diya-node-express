const { response } = require('express');
const sql = require('./db.js')

const Customer = function(customer){
    this.email = customer.email;
    this.name = customer.name;
    this.active = customer.active;
    this.tgl = customer.tgl
}

Customer.create = (newCustomer, result) =>{
    sql.query("insert into customers set ?", newCustomer, (err, res)=>{
        if(err){
            console.log(`error:${err}`);
            result(err, null);
            return;
        }
        //console.log(`created customer: ${{id:res.insertId}}`);
        result(null, {id:res.insertId, ...newCustomer});
    });
};

Customer.findById = (customerId, result) =>{
    sql.query(`select * from customers where id=${customerId}`, (err, res)=>{
    
        if(err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if(res.length){
            console.log('found customers: ', res[0]);
            result(null, res[0])
            return
        }
        result({kind:'not_found'}, null)
    });
};

Customer.getAll = result =>{
    sql.query('select * from customers', (err, res)=>{
        if(err){
            console.log(`error:${err}`)
            result(null, err)
            return
        }
        //console.log('customers: ', res);
        result(null, res)
    })
};

Customer.updateById= (id, customer, result)=>{
    sql.query(
        "update customers set email=?, name=?, active=? where id=?",
        [customer.email, customer.name, customer.active, id],
        (err, res)=>{
            if(err){
                console.log(`error:${err}`)
                result(null, err)
                return
            }
            if(res.affectedRows===0){
                result({kind:"not_found"}, null)
                return
            }
           console.log(`updated customer: `, {id:id, ...customer})
           result(null, {id:id, ...customer}) 
        }
    )
}

Customer.delete = (id, result)=>{
    sql.query('delete from customers where id=?', id, (err, res)=>{
        if(err){
            console.log('error', err)
            result(err, null)
            return
        }
        if(res.affectedRows==0){
            result({kind:"not_found"}, null)
            return
        }
        console.log("deleted customer with id: ", id);
        result(null, res);
    })
}

module.exports = Customer