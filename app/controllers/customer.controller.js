const Customer = require('../models/customer.model');

exports.create = (req, res)=>{
    if(!req.body){
        res.status(400).send(
            {
                msg:'canot empety'
            }
        )
    }
    const customer = new Customer({
        email:req.body.email,
        name:req.body.name,
        active: req.body.active,
        tgl:req.body.tgl
    })

    Customer.create(customer, (err, data)=>{
        if(err){
            res.status(500).send({
                message:'uups error'
            });
        }else{
            console.log(data)
            res.send(data)
        }
    })
}

exports.findAll = (req, res)=>{
    Customer.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving customers."
          });
        
        else {

            data.map(d=>{
                console.log(d.id)
                console.log(d.name)
            })
            res.send(data)
            
        };
      });
}

exports.findOne = (req, res)=>{
    Customer.findById(req.params.customersId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Customer with id ${req.params.customersId}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Customer with id " + req.params.customersId
            });
          }
        } else{
            res.send(data);
        }
      });
}

exports.update = (req, res)=>{
    if(!req.body){
        res.status(400).send({
            message:'Content can not be empty!'
        })
    }

    Customer.updateById(req.params.customersId, new Customer(req.body), (err, data)=>{
        if(err){
            if(err.kind==='not_found'){
                res.status(404).send({
                    msg:'not found'
                })
            }else{
                res.status(500).send({
                    msg:`error updating customers with id: ${req.params.customersId}.`
                })
            }
        }else res.send(data)
    })
}

exports.delete = (req, res)=>{
    Customer.delete(req.params.customersId, (err, data)=>{
        if(err){
            if(err.kind==='not_found'){
                res.status(400).send({
                    msg:'not found'
                })
            }else{
                res.status(500).send({
                    msg:'errors...'
                })
            }
        }else res.send({msg:'deleted success'})
    })
}