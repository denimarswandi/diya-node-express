module.exports = app =>{
    const customers = require("../controllers/customer.controller.js");
    app.post('/customers', customers.create);
    app.get('/customers', customers.findAll);
    app.get('/customers/:customersId', customers.findOne)
    app.put('/customers/:customersId', customers.update)
    app.delete('/customers/:customersId', customers.delete)
}